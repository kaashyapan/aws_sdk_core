defmodule AwsSdk.Client do
  @moduledoc """
  Documentation for AwsSdk.
  """
  @type client() :: map()

  @doc """
  Hello world.

  ## Examples

      iex> AwsSdk.hello()
      :world

  """
  def client() do
    :world
  end
end
