defmodule AwsSdk.Core do
  @moduledoc """
  Documentation for AwsSdk.
  """

  @type aws_error :: map()
  @type client :: AwsSdk.Client.client()
  @type config :: AwsSdk.Config.config()

  defdelegate config(), to: AwsSdk.Config
  defdelegate client(), to: AwsSdk.Client

  @spec make_request(map(), String.t(), map()) :: {:ok, map()} | aws_error
  def make_request(_client = %{}, request, _attrs = %{}) do
    if request == nil do
      {:error, %{error: "AWS error"}}
    else
      {:ok, nil}
    end
  end
end
