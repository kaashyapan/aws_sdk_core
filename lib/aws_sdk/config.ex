defmodule AwsSdk.Config do
  @moduledoc """
  Documentation for AwsSdk.
  """
  @type config() :: map()

  @doc """
  Hello world.

  ## Examples

      iex> AwsSdk.hello()
      :world

  """
  def config() do
    :world
  end
end
