{application,aws_sdk,
             [{applications,[kernel,stdlib,elixir,logger]},
              {description,"aws_sdk"},
              {modules,['Elixir.AwsSdk','Elixir.AwsSdk.Client',
                        'Elixir.AwsSdk.Config']},
              {registered,[]},
              {vsn,"0.1.0"}]}.
